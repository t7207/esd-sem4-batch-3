from flask import Flask, jsonify, request
from flask_cors import CORS
import pymysql

app = Flask(__name__)
cors = CORS(app)



@app.route('/sqr', methods=['GET'])
def getSqr():
    num1 = int(request.args.get('num1'));
    return f"Square of {num1} is {num1 * num1}"


@app.route('/add', methods=['GET'])
def add():
	num1 = int(request.args.get('num1'));
	num2 = int(request.args.get('num2'));
	return f"addition of {num1} + {num2} is {num1 + num2}"
	

@app.route('/sub', methods=['GET'])
def sub():
	num1 = int(request.args.get('num1'));
	num2 = int(request.args.get('num2'));
	return f"substraction of {num1} - {num2} is {num1 - num2}"


@app.route('/mul', methods=['PUT'])
def mul():
	num1 = int(request.args.get('num1'));
	num2 = int(request.args.get('num2'));
	return f"Multiplication of {num1} * {num2} is {num1 * num2}"

@app.route('/div', methods=['DELETE'])
def div():
	num1 = int(request.args.get('num1'));
	num2 = int(request.args.get('num2'));
	return f"division of {num1} / {num2} is {num1 / num2}"




if __name__ == "__main__":
    app.run(debug=True)